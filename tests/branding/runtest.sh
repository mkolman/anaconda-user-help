#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /anaconda-user-help.git/branding
#   Description: Anaconda user help branding test
#   Author: Peter Kotvan <pkotvan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
#    rlPhaseStartSetup
#    rlPhaseEnd

    rlPhaseStartTest
        rlLog "Verify whether anaconda user help contains the string fedora."
        rlRun -s "rpm -ql anaconda-user-help | xargs grep -irq fedora" 123
        if [[ $? -ne 123 ]]
        then
            rlFileSubmit ${rlRun_LOG} "fedora_string.log"
        fi
    rlPhaseEnd

#    rlPhaseStartCleanup
#    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
